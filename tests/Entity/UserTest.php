<?php

namespace App\Tests\Entity;

use App\Entity\Task;
use App\Entity\User;
use App\Repository\UserRepository;
use ContainerGIXL5o2\getDatabaseToolCollectionService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserTest extends KernelTestCase
{
    public function getHasheur()
    {
        return static::getContainer()->get(UserPasswordHasherInterface::class);
    }

    public function getUser(): User
    {
        $user = new User();
        $user->setEmail("test@example.com")
        ->setPassword($this->getHasheur()->hashPassword($user, "testmdp"))
        ->setRoles(["ROLE_USER"])
        ->setUsername("test");

        return $user;
    }

    public function countError($entity)
    {
        self::bootKernel();
        return static::getContainer()->get('validator')->validate($entity);
    }

    public function testValidEntity()
    {
        self::bootKernel();
        $error = static::getContainer()->get('validator')->validate($this->getUser());

        $this->assertCount(0, $error);
    }

    public function testInvalidEntity()
    {
        $user = $this->getUser();

        $user->setEmail("test")
            ->setUsername("t");

        $this->assertCount(2, $this->countError($user));
    }

    public function testAuthorTask()
    {
        $task = new Task();
        $task->setCreatedAt(new \DateTime())
            ->setIsDone(0)
            ->setTitle("test title")
            ->setContent("test de contenu plus long que le contenu de titre");
        
        $user = $this->getUser();
        $user->addTask($task);

        $this->assertEquals($user, $task->getOwner());
    }

    public function testUserRepository()
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $id = 1;
        $user = $userRepository->remove($userRepository->findOneBy(["id" => $id]));

        $removedUser = $userRepository->findOneBy(["id" => $id]);

        $this->assertSame($removedUser, null);
    }
}
