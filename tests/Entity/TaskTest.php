<?php

namespace App\Tests\Entity;

use App\Entity\Task;
use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Response;

class TaskTest extends KernelTestCase
{
    public function testValidEntity() {
        $task = new Task();
        $task->setCreatedAt(new \DateTime())
            ->setIsDone(0)
            ->setTitle("test title")
            ->setContent("test de contenu plus long que le contenu de titre");
        
        self::bootKernel();
        $error = static::getContainer()->get('validator')->validate($task);

        $this->assertCount(0, $error);
    }

    public function testInvalidEntity()
    {
        $task = new Task();
        $task->setCreatedAt(new \DateTime())
            ->setIsDone(0)
            ->setTitle("")
            ->setContent("test");

        self::bootKernel();
        $error = static::getContainer()->get('validator')->validate($task);

        $this->assertCount(3, $error);
    }

    public function testTaskIsDone()
    {
        $task = new Task();
        $task->setCreatedAt(new \DateTime())
            ->setIsDone(0)
            ->setTitle("test title")
            ->setContent("test de contenu plus long que le contenu de titre");

        $task->toggle(1);

        $this->assertEquals(1, $task->getIsDone());
    }

    public function testTaskRepository()
    {
        $taskRepository = static::getContainer()->get(TaskRepository::class);
        $id = 1;
        $task = $taskRepository->remove($taskRepository->findOneBy(["id" => $id]));

        $removedTask = $taskRepository->findOneBy(["id" => $id]);

        $this->assertSame($removedTask, null);
    }
}