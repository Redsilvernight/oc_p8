<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TaskControllerTest extends WebTestCase
{
    public function testFormAddTask()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('redsilvernight@gmail.com')[0];

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks/create');

        $form = $crawler->selectButton('Ajouter')->form();
        $form['task[title]'] = "test du titre";
        $form['task[content]'] = "contenu du contenu";

        $client->submit($form);
        $client->followRedirect();

        $this->assertSelectorTextContains('div.alert.alert-success', 'Superbe ! La tâche a été bien été ajoutée.');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testListDoneTask()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('redsilvernight@gmail.com')[0];

        $client->loginUser($testUser);

        $client->request('GET', '/tasks/done');

        $this->assertSelectorTextContains('section>h1', 'Liste des tâches terminées');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testTaskDone()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('redsilvernight@gmail.com')[0];

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks');

        $button  = $crawler->selectButton("Marquer comme faite")->form();
        $client->click($button);
        $client->followRedirect();

        $this->assertSelectorExists('div.alert.alert-success');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testEditTask() {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('redsilvernight@gmail.com')[0];

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks/2/edit');

        $form = $crawler->selectButton('Modifier')->form();
        $form['task[title]'] = "test du Modifié";
        $form['task[content]'] = "contenu du contenu modifié";

        $client->submit($form);
        $client->followRedirect();

        $this->assertSelectorTextContains('div.alert.alert-success', 'Superbe ! La tâche a bien été modifiée.');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testDeleteTask()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('redsilvernight@gmail.com')[0];

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks/2/delete');

        $client->followRedirect();

        $this->assertSelectorTextContains('div.alert.alert-success', 'Superbe ! La tâche a bien été supprimée.');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
