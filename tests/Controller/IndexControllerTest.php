<?php

namespace App\Test\Controller;

use App\DataFixtures\AppFixtures;
use App\Entity\User;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Exception;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class IndexControllerTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $container = $this->client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        $hasheur = $this->createMock(UserPasswordHasherInterface::class);

        $loader = new Loader();
        $loader->addFixture(new AppFixtures($hasheur));

        $purger = new ORMPurger($entityManager);
        $executor = new ORMExecutor($entityManager, $purger);
        $executor->execute($loader->getFixtures());

        $taskRepository = static::getContainer()->get(TaskRepository::class);
        $tasks = $taskRepository->findAll();

        $this->assertEquals(110, count($tasks));
    }

    public function testValidLogin()
    {

        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('redsilvernight@gmail.com')[0];

        $this->client->loginUser($testUser);
        
        $this->client->request('GET', '/');
        
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testFormInvalidLogin()
    {

        $crawler = $this->client->request('GET', '/login');

        $form = $crawler->selectButton('Connexion')->form();
        $form['email'] = "";
        $form['password'] = "";

        $this->client->submit($form);
        $this->client->followRedirect();

        $this->assertFormValue("form", 'password', "");
        $this->assertFormValue("form", 'email', "");
    }
}