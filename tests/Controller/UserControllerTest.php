<?php

namespace App\Test\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends WebTestCase
{
    public function testInvalidAuth()
    {
        $client = static::createClient();

        $client->request('GET', '/users');

        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testValidAuth()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('redsilvernight@gmail.com')[0];

        $client->loginUser($testUser);

        $client->request('GET', '/users');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testEditUser()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('redsilvernight@gmail.com')[0];

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/users/2/edit');

        $form = $crawler->selectButton('Modifier')->form();
        $form['registration_form[username]'] = "UserChange";

        $client->submit($form);
        $client->followRedirect();

        $this->assertSelectorTextContains('div.alert.alert-success', "L'utilisateur a bien été modifié");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testDeleteUser()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('redsilvernight@gmail.com')[0];

        $client->loginUser($testUser);

        $client->request('GET', '/users/2/delete');

        $client->followRedirect();

        $this->assertSelectorTextContains('div.alert.alert-success', "Superbe ! L'utilisateur a bien été supprimé.");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}