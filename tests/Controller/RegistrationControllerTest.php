<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class RegistrationControllerTest extends WebTestCase
{
    public function testFormValidRegister()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        
        $testUser = $userRepository->findOneByEmail('redsilvernight@gmail.com')[0];

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/register');

        $form = $crawler->selectButton('Inscription')->form();
        $form["registration_form[email]"] = "test@gmail.com";
        $form["registration_form[username]"] = "test";
        $form["registration_form[password]"] = "testtest";
        $form["registration_form[roles]"] = 0;

        $client->submit($form);
        $client->followRedirect();

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
