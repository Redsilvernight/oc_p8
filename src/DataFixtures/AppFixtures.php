<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function __construct(UserPasswordHasherInterface $hasheur)
    {
        $this->hasheur = $hasheur;
    }
    public function load(ObjectManager $manager): void
    {
        $this->faker = Factory::create('fr_FR');
        $this->manager = $manager;

        $this->loadUser();
        $this->loadTasks();
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }

    private function loadUser(): void
    {
        $this->loadMe();

        $roles = ["ROLE_ADMIN", "ROLLE_USER"];

        for ($u=0; $u < 20; $u++) { 
            $user = new User();
            $user->setEmail($this->faker->email())
                ->setUsername($this->faker->username())
                ->setRoles([$roles[array_rand($roles,1)]])
                ->setPassword($this->hasheur->hashPassword($user, $this->faker->password()));
            
                $this->manager->persist($user);

            for ($t = 0; $t < 5; $t++) {
                $task = $this->loadTasks();

                $this->manager->persist($task);

                if(mt_rand(0,5) != 4) {
                    $user->addTask($task);
                }
            }
        }
    }

    private function loadMe(): void
    {
        $user = new User();
        $user->setEmail("redsilvernight@gmail.com")
            ->setPassword($this->hasheur->hashPassword($user, "test"))
            ->setRoles(['ROLE_ADMIN'])
            ->setUsername("Redsilvernight");
        
            $this->manager->persist($user);

        for ($t = 0; $t < 10; $t++) {
            $task = $this->loadTasks();

            $this->manager->persist($task);

            $user->addTask($task);
        }
    }

    private function loadTasks(): Task
    {
        $task = new Task();
        $task->setTitle($this->faker->words($nb = 3, $asText = true))
            ->setContent($this->faker->sentence($nb=10))
            ->setCreatedAt(new \DateTime())
            ->setIsDone(mt_rand(0,1));
            
        return $task;
    }
}
