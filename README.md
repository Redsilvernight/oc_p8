# ToDO&Co

## Generals Infos
Projet n°8 de la formation open classroom PHP/Symfony. Une application de gestion de tâches.

## Technologies
***
This project was made with :
* [HTML]: Version 5
* [CSS]: Version 3
* [PHP]: Version 8.1
* [Symfony]: Version 6.1

* [BOOTSTRAP]: Version 5.1

## installation
***
Install this project with git and composer
***
* Clone repository
```
cd existing_repo
git remote add origin https://gitlab.com/Redsilvernight/oc_p8.git
git branch -M main
git push -uf origin main
```
* Update composer
```
$ composer update
```
* create database
```
$ symfony console doctrine:database:create
```
* Install database
```
$ symfony console doctrine:migration:migrate
```
* Load Fixtures
```
$ symfony console doctrine:fixtures:load
```
* Start symfony server
```
$ symfony server:start -d