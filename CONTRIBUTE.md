# Projet : Améliorez une application existante de ToDo & Co

## Guide de participation au projet.

# Avant propos
- Le projet fonctionne avec PHP 8.1
- Le projet est basé sur le framework symfony 6.1
- Cloner le projet : https://gitlab.com/Redsilvernight/oc_p8.git

# Contribuer au projet

1. Cloner le projet en suivant le README.md
2. Créez une branche à partir de *main* : git checkout -b newBranchName
3. Ecriver une nouvelle issue, correspondant aux modifications que vous allez apporter
4. Veuillez à respectez les bonnes pratiques (PSR-1, PSR-2)
5. Ecrivez le commit, clair et précis, en détaillant les modifications, avant d'effectuer un Push de la branche : git push origin myBranch
5. Tenez à jour les issues en fonction des labels
5. Faites un *Pull Request* et attendez sa validation

# Les bonnes pratiques 

   #  1. le code
    Vous devez respecter :
    - Le PSR 2 au minimum
    - Les standards du code de Symfony (`https://symfony.com/doc/current/contributing/code/standards.html`)
    - Les conventions du code de Symfony (`https://symfony.com/doc/5.2/contributing/code/conventions.html`)
    - La methode de nommage BEM (`http://getbem.com/`)

   # 2. les bundles
    - Toute installation de bundle PHP doit se faire avec "Composer OBLIGATOIREMENT"

   # 3. Git
    Vous devez faire les choses dans cet ordre : 
    - Nouvelle branche à partir de main, avec un nomage approprié
    - Commit suffisament détaillé, clair et précis
    - Issue correctement commentées et documentées, avec une deadline mise en place
    - pull Request OBLIGATOIRE
    - Seul le chef de projet peu faire un "merge" sur "main" après vérification.
    - Faire un update sur le code principal : git pull origin main

   # 4) Tests unitaires et fonctionnels
    - Les test sont créés avec phpUnit
    - Toutes les nouvelles fonctionnalitées doivent être testées au fur et à mesure
    - Vous devez respecter un taux de couverture au delà de 70%

   # 5) Diagramme UML
    - Réalisez des diagrammes UML (UseCase, Class, Sequence, Database) pour les nouvelles fonctionnalités

   # 6) Architecture de fichier
    - Respectez l'architecture de symfony 6 pour vos fichiers PHP ( src\Controller\... )
    - Les vues devront être dans le repertoire templates.